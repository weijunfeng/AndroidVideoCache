package com.danikula.videocache.sample;

import android.app.Application;
import android.content.Context;
import android.os.Process;

import com.danikula.videocache.HttpProxyCacheServer;
import com.danikula.videocache.VideoCacheLog;

/**
 * @author Alexey Danilov (danikula@gmail.com).
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        VideoCacheLog.setEnableLog(true);
    }

    private HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy(Context context) {
        App app = (App) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(this)
                .cacheDirectory(Utils.getVideoCacheDir(this))
                .build();
    }

    public static void exit(Context context) {
        App app = (App) context.getApplicationContext();
        if (app.proxy != null) {
            app.proxy.shutdown();
            app.proxy = null;
        }
        Process.killProcess(Process.myPid());
        System.exit(0);
    }
}
