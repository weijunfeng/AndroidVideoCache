package com.danikula.videocache;

import android.util.Log;

/**
 * Created by weijunfeng on 2018/7/28.
 * Email : weijunfeng@myhexin.com
 */
public class VideoCacheLog {
    private static boolean enableLog = false;

    public static void setEnableLog(boolean enableLog) {
        VideoCacheLog.enableLog = enableLog;
    }

    public static void error(String tag, String msg, Throwable t) {
        if (!enableLog) {
            return;
        }
        Log.e(tag, msg, t);
    }

    public static void error(String tag, String msg) {
        if (!enableLog) {
            return;
        }
        Log.e(tag, msg);
    }

    public static void info(String tag, String msg) {
        if (!enableLog) {
            return;
        }
        Log.i(tag, msg);
    }

    public static void warn(String tag, String msg) {
        if (!enableLog) {
            return;
        }
        Log.w(tag, msg);
    }

    public static void warn(String tag, String msg, Throwable t) {
        if (!enableLog) {
            return;
        }
        Log.w(tag, msg, t);
    }

    public static void debug(String tag, String msg) {
        if (!enableLog) {
            return;
        }
        Log.d(tag, msg);
    }
}
